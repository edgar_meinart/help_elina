class HomeController < ApplicationController
  before_action :set_lang

  def index
  end

  def set_lv
    session[:lang] = :lv
    redirect_to root_path
  end

  def set_rus
    session[:lang] = :rus
    redirect_to root_path
  end

  private

    def set_lang
      session[:lang] ||= :rus
      @language = session[:lang]
    end

end
